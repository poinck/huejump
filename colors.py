import colorsys
from PIL import ImageColor
MAX_HUE = 65535

# connect to OpenRGB
from openrgb import OpenRGBClient
from openrgb.utils import RGBColor, DeviceType

# connect to Philips Hue
import requests as rq

#def rgbh_to_hsvp(hex: str):
#    h, s, v = ImageColor.getcolor(hex, "RGB")
#    h = int(h / 360 * MAX_HUE)
#
#    return h, s, v

def rgbh_to_hsvp(hex: str):
    r, g, b = ImageColor.getcolor(hex, "RGB")
    print("RGB: " + str(r) + " " + str(g) + " " + str(b))
    h, s, v = colorsys.rgb_to_hsv(r / 255, g / 255, b / 255)
    print("HSV: " + str(h) + " " + str(s) + " " + str(v))
    h = int(h / 360 * MAX_HUE)
    s = int(s * 255)
    v = int(v * 255)
    print("HSV (transformed): " + str(h) + " " + str(s) + " " + str(v))

    return h, s, v

def send_to_openrgb(col, sat, bri):
    client = OpenRGBClient()
    print(client)
    motherboard = client.get_devices_by_type(DeviceType.MOTHERBOARD)[0]
    mouse = client.get_devices_by_type(DeviceType.MOUSE)[0]
    print(motherboard)
    print(mouse)
    motherboard.set_mode("direct")
    mouse.set_mode("direct")

    hue = int(col / MAX_HUE * 360)
    satp = int(sat / 255 * 100)
    brip = int(bri / 255 * 100)
    print("HSV (retransformed): " + str(hue) + " " + str(satp) + " " + str(brip))
    motherboard.set_color(RGBColor.fromHSV(hue, satp, brip))
    mouse.set_color(RGBColor.fromHSV(hue, satp, brip))

    client.disconnect()

# send to openrgb
def s2o(hue, sat, bri):
    client = OpenRGBClient()
    print(client)
    motherboard = client.get_devices_by_type(DeviceType.MOTHERBOARD)[0]
    mouse = client.get_devices_by_type(DeviceType.MOUSE)[0]
    print(motherboard)
    print(mouse)
    motherboard.set_mode("direct")
    mouse.set_mode("direct")

    motherboard.set_color(RGBColor.fromHSV(hue, sat, bri))
    mouse.set_color(RGBColor.fromHSV(hue, sat, bri))

    client.disconnect()

# convert to philips hue
def c4p(hue, sat, bri):
    c = hue / 360 * MAX_HUE
    s = sat / 100 * 255
    b = bri / 100 * 255

    return c, s, b

# convert to philips ct
def c4c(hue, bri):
    ct = 360 - hue
    b = bri / 100 * 255 / 5

    return ct, b

# send to philips hue
def s2p(hue, sat, bri):

    col, sat, bri = c4p(hue, sat, bri)

    tt = "10"
    data = '{"hue":' + str(int(col)) + ', "bri":' + str(int(bri)) + ', "sat":' + str(int(sat)) + ', "transitiontime":' + tt + '}'

    print(data)
    r = rq.put("http://192.168.2.17/api/" +
        "lampe-bash/lights/1/state",
        data = data)
    print(r.text)

# send to philips ct
def s2c(sat, bri):

    ct, bri = c4c(sat, bri)

    tt = "10"
    data = '{"ct":' + str(int(ct)) + ', "bri":' + str(int(bri)) + ', "transitiontime":' + tt + '}'

    print(data)
    r = rq.put("http://192.168.2.17/api/" +
        "lampe-bash/lights/7/state",
        data = data)
    print(r.text)

# type O
#h, s, v = 194, 28, 100
#c, t = 224, 48

# type B
#h, s, v = 183, 8, 100
#c, t = 223, 38

# type A
#h, s, v = 233, 4, 100
#c, t = 243, 34

# type F
#h, s, v = 36, 53, 97
#c, t = 66, 3

# type K
#h, s, v = 36, 73, 100
#c, t = 66, 13

# type M
#h, s, v = 26, 83, 100
#c, t = 46, 33

# type L
#h, s, v = 346, 97, 89
#c, t = 346, 97

# type T, S
#h, s, v = 332, 89, 60
#c, t = 322, 89

# tauri
#h, s, v = 26, 83, 82
#c, t = 46, 43

# type Y
#h, s, v = 312, 89, 30
#c, t = 302, 89

# type C
h, s, v = 16, 73, 62
c, t = 16, 53

# type AEBE, CN, CJ, MS
#h, s, v = 26, 63, 100
#c, t = 46, 13

# wolf-rayet, type WC
#h, s, v = 16, 34, 100
#c, t = 243, 34

# white-dwarf
#h, s, v = 242, 34, 99
#c, t = 243, 74

# neutron
#h, s, v = 242, 74, 99
#c, t = 243, 94

# black hole
#h, s, v = 152, 100, 10
#c, t = 192, 100

# port military
h, s, v = 0, 100, 50
c, t = 0, 100

# port refinery
h, s, v = 10, 100, 50
c, t = 25, 100

# port industrial
h, s, v = 25, 100, 50
c, t = 50, 100

# port extraction
h, s, v = 37, 100, 50
c, t = 75, 100

# port colony
h, s, v = 275, 100, 50
c, t = 275, 100

# carrier
h, s, v = 275, 35, 50
c, t = 255, 65

# port agri
h, s, v = 120, 100, 50
c, t = 170, 100

# port tourism
h, s, v = 245, 100, 50
c, t = 255, 100

# port high-tech
h, s, v = 175, 100, 50
c, t = 215, 100

# type G
#h, s, v = 26, 63, 100
#c, t = 46, 13

s2o(h, s, v)
s2p(c, t, v)
s2c(c, v)


#h, s, v = rgbh_to_hsvp("#f342aa")
#print(h)
#print(s)
#print(v)
