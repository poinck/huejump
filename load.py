import tkinter as tk
from tkinter import ttk
import myNotebook as nb
from config import config
from typing import Optional

from typing import Any, Mapping, MutableMapping, Optional, Tuple, Dict
import requests as rq
from queue import Queue
from threading import Thread

my_setting: Optional[tk.IntVar] = None
huejump_queue: Queue = Queue()

import colorsys
from PIL import ImageColor
MAX_HUE = 65535

# connect to OpenRGB
from openrgb import OpenRGBClient
from openrgb.utils import RGBColor, DeviceType

def plugin_start3(plugin_dir: str) -> str:
    """
    load huejump-plugin into EDMC
    """
    print("huejump loaded, plugin_dir = " + plugin_dir)

    huejump_thread = Thread(target=worker, name='huejump worker')
    huejump_thread.daemon = True
    huejump_thread.start()

    entry = "start"
    huejump_queue.put(entry)


    return "huejump"

def plugin_stop() -> None:
    print("signalling queue to close ..")
    huejump_queue.put(None)
    #huejump_thread.join()
    #huejump_thread = None

# For compatibility with pre-5.0.0
if not hasattr(config, 'get_int'):
    config.get_int = config.getint

if not hasattr(config, 'get_str'):
    config.get_str = config.get

if not hasattr(config, 'get_bool'):
    config.get_bool = lambda key: bool(config.getint(key))

if not hasattr(config, 'get_list'):
    config.get_list = config.get

def plugin_prefs(parent: nb.Notebook, cmdr: str, is_beta: bool) -> Optional[tk.Frame]:
    """
    Return a TK Frame for adding to the EDMC settings dialog.
    """
    global bridge_ip
    global bridge_id
    global brightness
    global primary_light_id
    global secondary_light_id

    # retrieve saved value from config
    bridge_ip = tk.StringVar(value=config.get_str("bridge_ip"))
    bridge_id = tk.StringVar(value=config.get_str("bridge_id"))
    brightness = tk.IntVar(value=config.get_int("brightness"))
    primary_light_id = tk.IntVar(value=config.get_int("primary_light_id"))
    secondary_light_id = tk.IntVar(value=config.get_int("secondary_light_id"))

    frame = nb.Frame(parent)
    frame.columnconfigure(1, weight=1)
    nb.Label(frame,
        text="o7 CMDR, connect ED with your Hue-Bridge here.").grid(
        padx=10, row=8, sticky=tk.W)

    nb.Label(frame, text="Hue-bridge IP-address:").grid(
        padx=10, row=11, sticky=tk.W)
    nb.Entry(frame, textvariable=bridge_ip).grid(
        padx=10, row=11, column=1, sticky=tk.EW)

    nb.Label(frame, text="Hue-bridge user-ID:").grid(
        padx=10, row=12, sticky=tk.W)
    nb.Entry(frame, textvariable=bridge_id).grid(
        padx=10, row=12, column=1, sticky=tk.EW)

    nb.Label(frame, text="Initial brightness:").grid(
        padx=10, row=13, sticky=tk.W)
    nb.Entry(frame, textvariable=brightness).grid(
        padx=10, row=13, column=1, sticky=tk.EW)

    nb.Label(frame, text="Primary Light-ID (HUE):").grid(
        padx=10, row=14, sticky=tk.W)
    nb.Entry(frame, textvariable=primary_light_id).grid(
        padx=10, row=14, column=1, sticky=tk.EW)

    nb.Label(frame, text="Secondary Light-ID (CT):").grid(
        padx=10, row=15, sticky=tk.W)
    nb.Entry(frame, textvariable=secondary_light_id).grid(
        padx=10, row=15, column=1, sticky=tk.EW)

    return frame

def prefs_changed(cmdr: str, is_beta: bool) -> None:
    """
    Save settings.
    """
    config.set('bridge_ip', bridge_ip.get())
    config.set('bridge_id', bridge_id.get())
    config.set('brightness', brightness.get())
    config.set('primary_light_id', primary_light_id.get())
    config.set('secondary_light_id', secondary_light_id.get())

    huejump_queue.put("config")

# send to openrgb
def s2o(hue, sat, bri):
    client = OpenRGBClient()
    print(client)
    motherboard = client.get_devices_by_type(DeviceType.MOTHERBOARD)[0]
    mouse = client.get_devices_by_type(DeviceType.MOUSE)[0]
    print(motherboard)
    print(mouse)
    motherboard.set_mode("direct")
    mouse.set_mode("direct")

    motherboard.set_color(RGBColor.fromHSV(hue, sat, bri))
    mouse.set_color(RGBColor.fromHSV(hue, sat, bri))

    client.disconnect()

# convert to philips hue
def c4p(hue, sat, bri):
    c = hue / 360 * MAX_HUE
    s = sat / 100 * 255
    b = bri / 100 * 255

    return c, s, b

# convert to philips ct
def c4c(hue, bri):
    ct = 360 - hue
    b = bri / 100 * 255 / 5

    return ct, b

def worker() -> None:
    print(".. huejump worker start")

    while True:
        item: str = huejump_queue.get()
        if item:
            entry = item

        try:
            tt = "5"
            bridge_ip = config.get_str("bridge_ip")
            bridge_id = config.get_str("bridge_id")
            brightness = config.get_int("brightness")
            primary_light_id = config.get_int("primary_light_id")
            secondary_light_id = config.get_int("secondary_light_id")

            print("huejump " + entry + " ..")
            h, c = 10, 30
            s, t = 100, 100
            v = 50
            if entry == "start":
                r = rq.put("http://" + bridge_ip + "/api/" +
                    bridge_id + "/lights/" + str(primary_light_id) + "/state",
                    data = '{"alert":"select"}')
            elif entry == "config":
                bri = brightness
            elif entry == "A":
                h, s, v = 233, 4, 100
                c, t = 243, 34
                tt = "120"
            elif entry == "M":
                h, s, v = 26, 83, 100
                c, t = 46, 33
                tt = "120"
            elif entry == "O":
                h, s, v = 194, 28, 100
                c, t = 224, 48
                tt = "120"
            elif entry == "B":
                h, s, v = 183, 8, 100
                c, t = 223, 38
                tt = "120"
            elif entry == "F":
                h, s, v = 36, 53, 97
                c, t = 66, 3
                tt = "120"
            elif entry == "G":
                h, s, v = 26, 63, 100
                c, t = 46, 13
                tt = "120"
            elif entry == "K":
                h, s, v = 36, 73, 100
                c, t = 66, 13
                tt = "120"
            elif entry == "L":
                h, s, v = 346, 97, 89
                c, t = 346, 97
                tt = "120"
            elif entry == "T":
                h, s, v = 332, 89, 60
                c, t = 322, 89
                tt = "120"
            #elif entry == "T Tauri":
            elif entry == "TTS":
                h, s, v = 26, 83, 82
                c, t = 46, 43
                tt = "120"
            elif entry == "Y":
                h, s, v = 312, 89, 30
                c, t = 302, 89
                tt = "120"
            elif entry == "C":
                h, s, v = 16, 73, 62
                c, t = 16, 53
                tt = "120"
            elif entry == "S":
                h, s, v = 332, 89, 60
                c, t = 322, 89
                tt = "120"
            elif entry == "WC":
                h, s, v = 16, 34, 100
                c, t = 243, 34
                tt = "120"
            elif entry == "CN":
                h, s, v = 26, 63, 100
                c, t = 46, 13
                tt = "120"
            elif entry == "CJ":
                h, s, v = 26, 63, 100
                c, t = 46, 13
                tt = "120"
            elif entry == "MS":
                h, s, v = 26, 63, 100
                c, t = 46, 13
                tt = "120"
            #elif entry == "Neutron Star":
            elif entry == "N":
                h, s, v = 242, 74, 99
                c, t = 243, 94
                tt = "120"
            elif entry == "DCV":
                h, s, v = 242, 74, 99
                c, t = 243, 74
                tt = "120"
            elif entry == "White Dwarf":
                h, s, v = 242, 34, 99
                c, t = 243, 74
                tt = "120"
            #elif entry == "Black Hole":
            elif entry == "H":
                h, s, v = 152, 100, 10
                c, t = 192, 100
                tt = "120"
            elif entry == "$economy_Military;":
                # red
                h, s, v = 0, 100, 50
                c, t = 0, 100
            elif entry == "$economy_Refinery;":
                # orange-red
                h, s, v = 10, 100, 50
                c, t = 25, 100
            elif entry == "$economy_Industrial;":
                # orange
                h, s, v = 25, 100, 50
                c, t = 50, 100
            elif entry == "$economy_Extraction;":
                # yellow-orange
                h, s, v = 37, 100, 50
                c, t = 75, 100
            elif entry == "$economy_Colony;":
                # purple
                h, s, v = 275, 100, 50
                c, t = 275, 100
            elif entry == "$economy_Carrier;":
                # gray
                h, s, v = 275, 35, 50
                c, t = 255, 65
            elif entry == "$economy_Agri;":
                # green
                h, s, v = 120, 100, 50
                c, t = 170, 100
            elif entry == "$economy_Tourism;":
                # blue
                h, s, v = 245, 100, 50
                c, t = 255, 100
            elif entry == "$economy_HighTech;":
                # cyan
                h, s, v = 175, 100, 50
                c, t = 215, 100

            s2o(h, s, v)
            col, sat, bri = c4p(c, t, v)
            ct, bri2 = c4c(c, v)
            data = '{"hue":' + str(int(col)) + ', "bri":' + str(int(bri)) + ', "sat":' + str(int(sat)) + ', "transitiontime":' + tt + '}'
            data2 = '{"ct":' + str(int(ct)) + ', "bri":' + str(int(bri2)) + ', "transitiontime":' + tt + '}'

            print(data)
            print(data2)
            if entry not in ('start'):
                r = rq.put("http://" + bridge_ip + "/api/" +
                    bridge_id + "/lights/" + str(primary_light_id) + "/state",
                    data = data)
                print("huejump brigde response = " + r.text)
                r = rq.put("http://" + bridge_ip + "/api/" +
                    bridge_id + "/lights/" + str(secondary_light_id) + "/state",
                    data = data2)
                print("huejump brigde response = " + r.text)
        except Exception as e:
            print("huejump exception = " + str(e))

    print(".. huejump worker stop")

def journal_entry(
    cmdr: str, is_beta: bool, system: str, station: str, entry: Dict[str, Any], state: Dict[str, Any]
) -> None:
    if entry['event'] == 'StartJump':
        # we will arrive at a new system
        print("huejump startjump = " + str(entry))
        if 'StarClass' in entry:
            print("huejump star class = " + entry["StarClass"])
            huejump_queue.put(entry["StarClass"])
    elif entry['event'] == 'Docked':
        print("huejump docked = " + str(entry))
        if 'StationEconomy' in entry:
            print("huejump station economy = " + entry["StationEconomy"])
            huejump_queue.put(entry["StationEconomy"])

